package com.magupe.push.common.protocol.response;

import com.magupe.push.common.protocol.Command;
import com.magupe.push.common.protocol.Packet;

public class RPCMessageResponsePacket extends Packet{

	private boolean success;

    private String reason;
    
    private String userId;
    private String message;
    private String secret;
    private String contentId;
	private String source;
	private long msgId;
	
	public long getMsgId() {
		return msgId;
	}

	public void setMsgId(long msgId) {
		this.msgId = msgId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	@Override
	public Byte getCommand() {
		return Command.RPC_MESSAGE_RESPONSE;
	}
}
