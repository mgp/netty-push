package com.magupe.push.common.codec;

import com.magupe.push.common.protocol.PacketCodeC;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

public class Spliter extends LengthFieldBasedFrameDecoder {

	private static final int LENGTH_FIELD_OFFSET = 7;
    private static final int LENGTH_FIELD_LENGTH = 4;
    
    /**
     * 拆包器（解决粘包半包现象）
     */
    public Spliter() {
    	/**
    	 * 数据包的最大长度
    	 * 长度域相对整个数据包的偏移量(4 + 1 + 1 + 1)
    	 * 长度域(4)
    	 */
        super(Integer.MAX_VALUE, LENGTH_FIELD_OFFSET, LENGTH_FIELD_LENGTH);
    }
    
    /**
     * 判断当前发来的数据包是否是满足我的自定义协议
     */
	@Override
    protected Object decode(ChannelHandlerContext ctx, ByteBuf in) throws Exception {
        // in为数据包的开头，获取协议魔数，屏蔽非本协议的客户端
        if (in.getInt(in.readerIndex()) != PacketCodeC.MAGIC_NUMBER) {
            ctx.channel().close();
            return null;
        }

        return super.decode(ctx, in);
    }
}
