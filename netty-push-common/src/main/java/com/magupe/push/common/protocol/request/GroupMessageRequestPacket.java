package com.magupe.push.common.protocol.request;

import com.magupe.push.common.protocol.Command;
import com.magupe.push.common.protocol.Packet;

public class GroupMessageRequestPacket extends Packet{

	private String message;
	private String groupId;
	private String source;
	private long msgId;
	
	public long getMsgId() {
		return msgId;
	}

	public void setMsgId(long msgId) {
		this.msgId = msgId;
	}
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	@Override
	public Byte getCommand() {
		return Command.GROUP_MESSAGE_REQUEST;
	}
}
