package com.magupe.push.common.serializer;

import java.io.IOException;

import com.magupe.push.common.serializer.impl.FastJsonSerializer;

public interface Serializer {

	Serializer DEFAULT = new FastJsonSerializer();
	
    /**
     * 序列化算法
     */
    byte getSerializerAlogrithm();

    /**
     * java 对象转换成二进制
     * @throws JsonProcessingException 
     */
    byte[] serialize(Object object);

    /**
     * 二进制转换成 java 对象
     * @throws IOException 
     * @throws JsonMappingException 
     * @throws JsonParseException 
     */
    <T> T deserialize(Class<T> clazz, byte[] bytes);

}