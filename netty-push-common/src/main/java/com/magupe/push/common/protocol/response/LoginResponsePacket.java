package com.magupe.push.common.protocol.response;

import com.magupe.push.common.protocol.Command;
import com.magupe.push.common.protocol.Packet;

public class LoginResponsePacket extends Packet{

	private String contentId;
	
    private String secret;
    
	private boolean success;

    private String reason;
    
    private long msgId;
    
    private String userId;
	private String source;
	
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}
	public long getMsgId() {
		return msgId;
	}

	public void setMsgId(long msgId) {
		this.msgId = msgId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	@Override
	public Byte getCommand() {
		return Command.LOGIN_RESPONSE;
	}
}
