package com.magupe.push.common.serializer.impl;

import com.alibaba.fastjson.JSON;
import com.magupe.push.common.serializer.Serializer;
import com.magupe.push.common.serializer.SerializerAlgorithm;

public class FastJsonSerializer implements Serializer{

	@Override
	public byte getSerializerAlogrithm() {
		return SerializerAlgorithm.FASTJSON;
	}
	
	@Override
	public byte[] serialize(Object object) {
		return JSON.toJSONBytes(object);
	}

	@Override
	public <T> T deserialize(Class<T> clazz, byte[] bytes) {
		return JSON.parseObject(bytes, clazz);
	}
}
