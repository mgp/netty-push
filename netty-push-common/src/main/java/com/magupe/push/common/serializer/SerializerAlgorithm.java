package com.magupe.push.common.serializer;

public interface SerializerAlgorithm {

	/**
     * fastjson 序列化标识
     */
    byte FASTJSON = 1;
    
    /**
     * json 序列化标识
     */
    byte JSON = 2;
    
    /**
     * java 序列化标识
     */
    byte JAVA = 3;

    
    /**
     * protobuff 序列化标识
     */
	byte PROTOBUFF = 4;
    
}
