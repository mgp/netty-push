package com.magupe.push.common.protocol;

public interface Command {

	Byte LOGIN_REQUEST = 1;

    Byte LOGIN_RESPONSE = 2;
    
	Byte AUTHENTICATION_REQUEST = 3;

    Byte AUTHENTICATION_RESPONSE = 4;
    
    Byte MESSAGE_REQUEST = 5;
    
    Byte MESSAGE_RESPONSE = 6;
    
    Byte GROUP_MESSAGE_REQUEST = 7;
    
    Byte GROUP_MESSAGE_RESPONSE = 8;
    
    Byte RPC_MESSAGE_REQUEST = 9;
    
    Byte RPC_MESSAGE_RESPONSE = 10;
    
    Byte FILE_REQUEST = 11;
    
    Byte FILE_RESPONSE = 12;
}
