package com.magupe.push.common.attribute;

import com.magupe.push.common.session.Session;
import com.magupe.push.common.session.AuthenticationSession;

import io.netty.util.AttributeKey;

public interface Attributes {

	AttributeKey<Session> SESSION = AttributeKey.newInstance("session");
	
	AttributeKey<AuthenticationSession> AUTHENTICATIONSESSION = AttributeKey.newInstance("authenticationSession");
}
