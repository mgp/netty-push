package com.magupe.push.common.session;

public class AuthenticationSession {

	private String contentId;
	
    private String secret;
    
    public AuthenticationSession() {

    }
    
	public AuthenticationSession(String secret, String contentId) {
		this.contentId = contentId;
		this.secret = secret;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}
}
