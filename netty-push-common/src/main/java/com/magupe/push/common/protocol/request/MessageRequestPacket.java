package com.magupe.push.common.protocol.request;

import com.magupe.push.common.protocol.Command;
import com.magupe.push.common.protocol.Packet;

public class MessageRequestPacket extends Packet{

	private String secret;
	private String contentId;
	private String message;
	private String userId;
	
	private long msgId;
	private String source;
	
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}
	public String getSecret() {
		return secret;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public long getMsgId() {
		return msgId;
	}

	public void setMsgId(long msgId) {
		this.msgId = msgId;
	}

	@Override
	public Byte getCommand() {
		return Command.MESSAGE_REQUEST;
	}
}
