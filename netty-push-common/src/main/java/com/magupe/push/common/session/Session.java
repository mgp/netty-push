package com.magupe.push.common.session;

public class Session {

	private String contentId;
	
    private String secret;

    private String deviceId;
    
    private String userId;
    
    public Session() {

    }
    
	public Session(String secret, String contentId, String deviceId, String userId) {
		this.contentId = contentId;
		this.secret = secret;
		this.deviceId = deviceId;
		this.userId = userId;
	}

	public String getContentId() {
		return contentId;
	}

	public void setContentId(String contentId) {
		this.contentId = contentId;
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}
    
	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	@Override
    public String toString() {
        return "Session: {contentId: " + contentId + ", ssecret: " + secret+", deviceId: " + deviceId + ", userId: " + userId + "}";
    }
}
