package com.magupe.push.common.protocol;

import com.alibaba.fastjson.annotation.JSONField;

public abstract class Packet{

	/**
     * 协议版本
     */
	@JSONField(deserialize = false, serialize = false)
    private Byte version = 1;

	public Byte getVersion() {
		return version;
	}

	public void setVersion(Byte version) {
		this.version = version;
	}
	
    /**
     * 抽象的获取指令的方法
     */
	@JSONField(serialize = false)
    public abstract Byte getCommand();
}
