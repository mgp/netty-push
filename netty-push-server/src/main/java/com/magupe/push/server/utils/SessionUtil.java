package com.magupe.push.server.utils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.magupe.push.common.attribute.Attributes;
import com.magupe.push.common.session.Session;

import io.netty.channel.Channel;

public class SessionUtil {

	private static final Map<String, Channel> channelMap = new ConcurrentHashMap<>();
	
	public static void bindSession(Session session, Channel channel) {
		channelMap.put(session.getUserId(), channel);
		channel.attr(Attributes.SESSION).set(session);
	}

    public static void unBindSession(Channel channel) {
        if (hasLogin(channel)) {
        	channelMap.remove(getSession(channel).getUserId());
            channel.attr(Attributes.SESSION).set(null);
        }
    }
    
    public static boolean hasLogin(Channel channel) {
        return channel.hasAttr(Attributes.SESSION);
    }
    
    public static Session getSession(Channel channel) {
        return channel.attr(Attributes.SESSION).get();
    }

    public static Channel getChannel(String userId) {
        return channelMap.get(userId);
    }
}
