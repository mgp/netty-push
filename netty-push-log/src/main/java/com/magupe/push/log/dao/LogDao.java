package com.magupe.push.log.dao;

import java.util.List;
import java.util.Map;

import com.magupe.push.common.entity.Log;

public interface LogDao {

	public Log get(Long id);
	
	public int save(Log entity);
	
	public int update(Log entity);
	
	public int delete(Long id);
	
	List<Log> list(Map<String, Object> params);
}
