package com.magupe.push.log.util;

import java.security.SecureRandom;
import java.util.UUID;

public class IdGen {

	private static SecureRandom random = new SecureRandom();
	private static SnowflakeIdWorker snowFlake = new SnowflakeIdWorker(0, 0);
	
	public static long snowFlakeId() {
		return snowFlake.nextId();
	}
	
	public static String uuid() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
	public static long randomLong() {
		return Math.abs(random.nextLong());
	}

	public String getNextId() {
		return IdGen.uuid();
	}
}
