package com.magupe.push.log.util;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.magupe.push.common.entity.Log;
import com.magupe.push.common.protocol.Packet;
import com.magupe.push.common.protocol.request.AuthenticationRequestPacket;
import com.magupe.push.common.protocol.request.LoginRequestPacket;
import com.magupe.push.common.protocol.request.MessageRequestPacket;
import com.magupe.push.common.protocol.request.RPCMessageRequestPacket;
import com.magupe.push.common.protocol.response.AuthenticationResponsePacket;
import com.magupe.push.common.protocol.response.LoginResponsePacket;
import com.magupe.push.common.protocol.response.MessageResponsePacket;
import com.magupe.push.common.protocol.response.RPCMessageResponsePacket;
import com.magupe.push.log.service.LogService;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;

public class LogUtils {

	protected static Logger logger = LoggerFactory.getLogger(LogUtils.class.getClass());
	
	static LogService logService = new LogService();
	
	public static void processLog(Channel channel, Packet packet, String prompting, boolean isForward, String forward) {
		Log log = new Log();
		log.setId(IdGen.snowFlakeId());
		log.setMessage(JSON.toJSONString(packet));
		log.setPrompting(prompting);
		log.setForward(isForward);
		log.setForwardUrl(forward);
		log.setLocalAddress(channel.localAddress().toString());
		log.setRemoteAddress(channel.remoteAddress().toString());
		log.setPacket(packet.getClass().getName());
		log.setCreateDate(new Date());
		
		if(packet instanceof AuthenticationRequestPacket) {
			AuthenticationRequestPacket p = (AuthenticationRequestPacket) packet;
			log.setSecret(p.getSecret());
			log.setContentId(p.getContentId());
			log.setMsgId(p.getMsgId());
			log.setSource(p.getSource());
			
		} else if(packet instanceof AuthenticationResponsePacket) {
			AuthenticationResponsePacket p = (AuthenticationResponsePacket) packet;
			log.setSecret(p.getSecret());
			log.setContentId(p.getContentId());
			log.setMsgId(p.getMsgId());
			log.setSource(p.getSource());
			
		} else if(packet instanceof LoginRequestPacket) {
			LoginRequestPacket p = (LoginRequestPacket) packet;
			log.setSecret(p.getSecret());
			log.setContentId(p.getContentId());
			log.setUserId(p.getUserId());
			log.setMsgId(p.getMsgId());
			log.setSource(p.getSource());
			
		} else if(packet instanceof LoginResponsePacket) {
			LoginResponsePacket p = (LoginResponsePacket) packet;
			log.setSecret(p.getSecret());
			log.setContentId(p.getContentId());
			log.setUserId(p.getUserId());
			log.setMsgId(p.getMsgId());
			log.setSource(p.getSource());
			
		} else if(packet instanceof MessageRequestPacket) {
			MessageRequestPacket p = (MessageRequestPacket) packet;
			log.setSecret(p.getSecret());
			log.setContentId(p.getContentId());
			log.setUserId(p.getUserId());
			log.setMsgId(p.getMsgId());
			log.setSource(p.getSource());
			
		} else if(packet instanceof MessageResponsePacket) {
			MessageResponsePacket p = (MessageResponsePacket) packet;
			log.setSecret(p.getSecret());
			log.setContentId(p.getContentId());
			log.setUserId(p.getUserId());
			log.setMsgId(p.getMsgId());
			log.setSource(p.getSource());
			
		} else if(packet instanceof RPCMessageRequestPacket) {
			RPCMessageRequestPacket p = (RPCMessageRequestPacket) packet;
			log.setSecret(p.getSecret());
			log.setContentId(p.getContentId());
			log.setUserId(p.getUserId());
			log.setMsgId(p.getMsgId());
			log.setSource(p.getSource());
			
		} else if(packet instanceof RPCMessageResponsePacket) {
			RPCMessageResponsePacket p = (RPCMessageResponsePacket) packet;
			log.setSecret(p.getSecret());
			log.setContentId(p.getContentId());
			log.setUserId(p.getUserId());
			log.setMsgId(p.getMsgId());
			log.setSource(p.getSource());
		}
		
		save(log);
	}

	public static void processExceptionLog(ChannelHandlerContext ctx, Exception e, String prompting) {
		logger.error(prompting, e);
	}
	
	public static void save(Log log) {
		ExecutorsUtils.executor.submit(() -> {
			logService.save(log);
		});
	}
}
