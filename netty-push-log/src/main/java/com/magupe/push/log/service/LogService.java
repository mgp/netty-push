package com.magupe.push.log.service;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.magupe.push.common.entity.Log;
import com.magupe.push.log.dao.LogDao;
import com.magupe.push.log.util.MybatisUtils;

public class LogService {
	
	public Log get(Long id) {
		SqlSession session = null;
		try {
			session = MybatisUtils.openSession();
			LogDao logDao = session.getMapper(LogDao.class);
			return logDao.get(id);
		} finally {
			session.close();
		}
	}
	
	public void save(Log entity) {
		SqlSession session = null;
		try {
			session = MybatisUtils.openSession();
			LogDao logDao = session.getMapper(LogDao.class);
			logDao.save(entity);
			session.commit();
		} finally {
			session.close();
		}
	}
	
	public void update(Log entity) {
		SqlSession session = null;
		try {
			session = MybatisUtils.openSession();
			LogDao logDao = session.getMapper(LogDao.class);
			logDao.update(entity);
			session.commit();
		} finally {
			session.close();
		}
	}
	
	public void delete(Long id) {
		SqlSession session = null;
		try {
			session = MybatisUtils.openSession();
			LogDao logDao = session.getMapper(LogDao.class);
			logDao.delete(id);
			session.commit();
		} finally {
			session.close();
		}
	}
	
	public List<Log> list(Map<String, Object> params){
		SqlSession session = null;
		try {
			session = MybatisUtils.openSession();
			LogDao logDao = session.getMapper(LogDao.class);
			return logDao.list(params);
		} finally {
			session.close();
		}
	}
}
