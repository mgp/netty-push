-- ----------------------------
-- Table structure for `netty_push_log`
-- ----------------------------
DROP TABLE IF EXISTS `netty_push_log`;
CREATE TABLE `netty_push_log` (
  `id` bigint(20) NOT NULL,
  `secret` varchar(64) DEFAULT NULL COMMENT '秘钥',
  `content_id` varchar(64) DEFAULT NULL COMMENT '应用标示',
  `user_id` varchar(64) DEFAULT NULL COMMENT '信息接收人',
  `msg_id` bigint(20) NOT NULL COMMENT '信息id（链路联系时使用）',
  `message` varchar(2000) DEFAULT NULL COMMENT '信息',
  `prompting` varchar(128) DEFAULT NULL COMMENT '提示语',
  `forward_url` varchar(128) DEFAULT NULL COMMENT '转发链接',
  `is_forward` char(1) DEFAULT NULL COMMENT '是否是转发信息',
  `source` varchar(32) DEFAULT NULL COMMENT '信息来源',
  `packet` varchar(128) DEFAULT NULL COMMENT 'packet类型',
  `local_address` varchar(128) DEFAULT NULL COMMENT 'local_address',
  `remote_address` varchar(128) DEFAULT NULL COMMENT 'remote_address',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='统一日志';
