package com.magupe.push.client.handler;

import com.magupe.push.common.protocol.response.LoginResponsePacket;
import com.magupe.push.log.util.LogUtils;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class LoginResponseHandler extends SimpleChannelInboundHandler<LoginResponsePacket>{

	public LoginResponseHandler() {
		super();
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, LoginResponsePacket packet) throws Exception {
        if (packet.isSuccess()) {
        	LogUtils.processLog(ctx.channel(), packet, "移动端登录成功", false, null);
        } else {
        	LogUtils.processLog(ctx.channel(), packet, "移动端登录失败", false, null);
        }
	}
}
