package com.magupe.push.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magupe.push.client.handler.LoginResponseHandler;
import com.magupe.push.client.handler.MessageResponseHandler;
import com.magupe.push.client.handler.RPCMessageResponseHandler;
import com.magupe.push.common.codec.PacketDecoder;
import com.magupe.push.common.codec.PacketEncoder;
import com.magupe.push.common.codec.Spliter;
import com.magupe.push.common.protocol.request.LoginRequestPacket;
import com.magupe.push.log.util.IdGen;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

public class NettyPushClientSDK{

	protected Logger logger = LoggerFactory.getLogger(NettyPushClientSDK.class);
	
	private static final int MAX_RETRY = 3;
    private static final String HOST = "10.6.190.174";
    private static final int PORT = 7000;
	public static boolean isLogin = false;
    
	Channel channel;
	
	NettyPushClientSDK(){

	}
	
	protected void init() {
    	NioEventLoopGroup workerGroup = new NioEventLoopGroup();
    	
    	Bootstrap bootstrap = new Bootstrap();
        bootstrap
	        .group(workerGroup)
	        .channel(NioSocketChannel.class)
	        .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000)
	        .option(ChannelOption.SO_KEEPALIVE, true)
	        .option(ChannelOption.TCP_NODELAY, true)
	        .handler(new ChannelInitializer<SocketChannel>() {
	            @Override
	            public void initChannel(SocketChannel ch) {
	            	ch.pipeline().addLast(new Spliter());
	                ch.pipeline().addLast(new PacketDecoder());
	                ch.pipeline().addLast(new LoginResponseHandler());
	                ch.pipeline().addLast(new MessageResponseHandler());
	                ch.pipeline().addLast(new RPCMessageResponseHandler());
	                ch.pipeline().addLast(new PacketEncoder());
	            }
	        });

        ChannelFuture future = connectRetry(bootstrap, HOST, PORT, MAX_RETRY);
        if(future == null) {
        	System.exit(0);
        }
        channel = ((ChannelFuture) future).channel();
	}
    
	protected ChannelFuture connectRetry(Bootstrap bootstrap, String host, int port, int retry) {
        try {
        	return bootstrap.connect(HOST, PORT).sync();
		} catch (Exception e) {
			retry --;
			if(retry <= 0) {
				logger.error("Connect Netty Error", e);
				return null;
			}
			logger.info("retry");
			return connectRetry(bootstrap, host, port, retry);
		}
    }
    
    protected void login(String secret, String contentId, String userId) {
		LoginRequestPacket packet = new LoginRequestPacket();
		packet.setContentId(contentId);
		packet.setSecret(secret);
		packet.setUserId(userId);
		packet.setSource("mobile");
		packet.setMsgId(IdGen.snowFlakeId());
		
		channel.writeAndFlush(packet);
	}

	public static void main(String[] args) {
		String userId = "123456";
		if(args != null && args.length > 0) {
			userId = args[0];
		}
		
		String contentId = "com.magupe.push";
		String secret = "f988c38b94ec4e6fbf0115f12779ba09";
		NettyPushClientSDK client = new NettyPushClientSDK();
		client.init();
		client.login(secret, contentId, userId);
	}
}
