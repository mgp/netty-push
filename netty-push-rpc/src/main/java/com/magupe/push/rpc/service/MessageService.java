package com.magupe.push.rpc.service;

import com.magupe.push.common.protocol.request.RPCMessageRequestPacket;
import com.magupe.push.common.protocol.response.RPCMessageResponsePacket;

public interface MessageService {

	public RPCMessageResponsePacket sendMessage(RPCMessageRequestPacket message);
}
