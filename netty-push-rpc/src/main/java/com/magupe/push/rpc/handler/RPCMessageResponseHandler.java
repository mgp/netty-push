package com.magupe.push.rpc.handler;

import java.util.concurrent.CountDownLatch;

import com.magupe.push.common.protocol.response.RPCMessageResponsePacket;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class RPCMessageResponseHandler extends SimpleChannelInboundHandler<RPCMessageResponsePacket>{

	private RPCMessageResponsePacket result;
	private CountDownLatch countDownLatch;
	
	public RPCMessageResponseHandler() {
		countDownLatch = new CountDownLatch(1);
	}
	
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, RPCMessageResponsePacket packet) throws Exception {
		this.result = packet;
		countDownLatch.countDown();
	}
	
	public RPCMessageResponsePacket getResult() {
		try {
            countDownLatch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
		return result;
	}
}
