package com.magupe.push.client.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magupe.push.common.protocol.response.AuthenticationResponsePacket;
import com.magupe.push.log.util.LogUtils;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class AuthenticationResponseHandler extends SimpleChannelInboundHandler<AuthenticationResponsePacket>{

	protected Logger logger = LoggerFactory.getLogger(AuthenticationResponseHandler.class);
	
	public AuthenticationResponseHandler() {
		super();
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, AuthenticationResponsePacket packet) throws Exception {
		if (packet.isSuccess()) {
        	LogUtils.processLog(ctx.channel(), packet, "客户端认证成功", false, null);
        } else {
        	LogUtils.processLog(ctx.channel(), packet, "客户端认证失败", false, null);
        }
	}
}
