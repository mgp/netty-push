package com.magupe.push.client.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magupe.push.common.protocol.response.MessageResponsePacket;
import com.magupe.push.log.util.LogUtils;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class MessageResponseHandler extends SimpleChannelInboundHandler<MessageResponsePacket>{

	protected Logger logger = LoggerFactory.getLogger(MessageResponseHandler.class);
	
	public MessageResponseHandler() {
		super();
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, MessageResponsePacket packet) throws Exception {
        if (packet.isSuccess()) {
        	LogUtils.processLog(ctx.channel(), packet, "推送消息成功", false, null);
        } else {
        	LogUtils.processLog(ctx.channel(), packet, "推送消息失败", false, null);
        }
	}
}
