package com.magupe.push.client.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.magupe.push.common.protocol.response.RPCMessageResponsePacket;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

public class RPCMessageResponseHandler extends SimpleChannelInboundHandler<RPCMessageResponsePacket>{

	protected Logger logger = LoggerFactory.getLogger(RPCMessageResponseHandler.class);
	
	public RPCMessageResponseHandler() {
		super();
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, RPCMessageResponsePacket packet) throws Exception {
		logger.info("推送消息成功：" + JSON.toJSON(packet).toString());;
	}
}
